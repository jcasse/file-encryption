from helper import getCommandLineArgs
from aescrypto import encrypt_file, decrypt_file
from constants import ENCRYPT, DECRYPT

try:

    args = getCommandLineArgs()

    if args.action == ENCRYPT: encrypt_file(args.password, args.pathname)

    if args.action == DECRYPT: decrypt_file(args.password, args.pathname)

except Exception, e:
    print e.message
