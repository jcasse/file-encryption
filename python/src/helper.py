import os
import argparse
import textwrap
from constants import ACTIONS, ENCRYPT, DECRYPT
from constants import CRYPTOFILEEXTENSION

def getCommandLineArgs():
    '''
    Handles command-line arguments.
    :returns: (Namespace) processed command-line arguments
    '''
    DESCRIPTION = 'File encryption using AES CBC'
    parser = argparse.ArgumentParser(
            description = DESCRIPTION,
            formatter_class = argparse.RawDescriptionHelpFormatter,
            epilog = textwrap.dedent(
                    '''
                    Example Usage
                    -------------
                    python %(prog)s ../examples/data.txt abcd1234
                    python %(prog)s ../examples/data.enc abcd1234
                    '''
                    )
            )
    parser.add_argument('--action', default=None, choices=ACTIONS,
            help='placeholder, not required. the action is automatically \
                    selected according to the file extension in <pathname>')
    parser.add_argument('pathname', metavar = '<pathname>',
            help='pathname to file to encrypt/decrypt',
            action=ValidateFileAction)
    parser.add_argument('password', metavar = '<password>',
            help='password to use for encryption/decryption of file',
            action=ValidatePasswordAction)
    return parser.parse_args()

class ValidateFileAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None: raise ValueError("nargs not allowed")
        super(ValidateFileAction, self).__init__(option_strings, dest, **kwargs)
    def __call__(self, parser, namespace, values, option_string=None):
        pathname = values
        if not os.path.isfile(pathname):
            raise ValueError('{} is not a regular file'.format(pathname))
        if pathname.endswith(CRYPTOFILEEXTENSION): action = DECRYPT
        else: action = ENCRYPT
        if namespace.action is None: namespace.action = action
        else:
            if namespace.action != action:
                raise ValueError('action "{}" not compatible with file'.format(
                    namespace.action))
        setattr(namespace, self.dest, pathname)

class ValidatePasswordAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None: raise ValueError("nargs not allowed")
        super(ValidatePasswordAction, self).__init__(
                option_strings, dest, **kwargs)
    def __call__(self, parser, namespace, values, option_string=None):
        password = values
        #if len(password) < 8:
        #    raise ValueError('password must be at least 8 characters long')
        setattr(namespace, self.dest, password)
